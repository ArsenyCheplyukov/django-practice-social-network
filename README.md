## Name
# Django Practice Social Network

## Description
Simple social network application

<!-- ## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge. -->

<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation
- To get started after cloning the repository, you need to activate poetry venv.
  There are many ways to perform that, but i preffered the following:
  ```poetry shell```
  This activates poetry and allows to use it like venv in vanilla python
- After that move to main django file, where manage.py is located:
  ```cd bookmark```
- There can be a possibility to use migrate. To do this use following:
  ```python manage.py makemigration account```
  ```python namage.py migrate```
- Also you need to add superuser, this can be performed via:
  ```python manage.py createsuperuser```
- After adding superuser, you need to add simple user, this can be performed by moving into this url:
  ```localhost:8000/admin/``` and add users manualy
- To run server use the following command:
  ```python manage.py runserver```

<!-- ## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->

<!-- ## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc. -->

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## How this performed:
  - #### How to add photos:
    - Add photo field in model, like this: ```models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)```
    -  *If you want to add this model to user auth you need to make a new model and add link (as one-to-one field) to this model, like this:*
        ```python
        class Profile(models.Model):
            user = models.OneToOneField(settings.   AUTH_USER_MODEL, on_delete=models.CASCADE)
            date_of_birth = models.DateField(blank=True, null=True)
            photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
            def __str__(self):
                return f'Profile of {self.user.username}'
        ```
    
    - Add media links to settings.py: ```MEDIA_URL = 'media/'      MEDIA_ROOT = BASE_DIR / 'media'```
    - Add static file link in debug mode in url.py: 
        ```if settings.DEBUG:```
        ```urlpatterns += static(settings.MEDIA_URL,    document_root=settings.MEDIA_ROOT)```
    - Edit admin panel, add registration for this model to edit it simply:
        ```python
        @admin.register(Profile)
            class ProfileAdmin(admin.ModelAdmin):
                list_display = ['user', 'date_of_birth', 'photo']
                raw_id_fields = ['user']
        ```


## Authors and acknowledgment
Arseny Cheplyukov, projects taken from the book "Django 4 in examples" by Antonio Mele

## License
MIT licensed

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
