"""Create forms for sending data into server"""
from django import forms
from django.contrib.auth.models import User
from .models import Profile


class LoginForm(forms.Form):
    """Login into account and creating new"""
    username = forms.CharField()
    # passing widget allowing to determine is this password 
    # by keyword type="password" in html
    password = forms.CharField(widget=forms.PasswordInput)


class UserRegistrationForm(forms.ModelForm):
    """Form for creating new user login data"""
    # create password field
    password = forms.CharField(label='Password',
                               widget=forms.PasswordInput)
    # repeat password for aprove
    password2 = forms.CharField(label='Repeat Password',
                                widget=forms.PasswordInput)

    class Meta:
        """Modify user behavior"""
        model = User
        fields = ['username', 'first_name', 'email']

    def clean_password2(self):
        """Check if two passwords are equal"""
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError("Passwords don't match.")
        return cd['password2']
    
    def clean_email(self):
        """Make possibility to use only one email address (unique)"""
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError('Email already in use.')
        return data


class UserEditForm(forms.ModelForm):
    """Allows users to edit their first name, last name and email"""
    class Meta:
        """Point model and fields to fill"""
        model = User
        fields = ['first_name', 'last_name', 'email']
    
    def clean_email(self):
        """Allow to use only one unique email address"""
        data = self.cleaned_data['email']
        qs = User.objects.exclude(id=self.instance.id)\
            .filter(email=data)
        if qs.exists():
            raise forms.ValidationError(' Email already in use.')
        return data


class ProfileEditForm(forms.ModelForm):
    """Allows user to edit their data in current Profile model"""
    class Meta:
        """Point model and fields to fill"""
        model = Profile
        fields = ['date_of_birth', 'photo']
