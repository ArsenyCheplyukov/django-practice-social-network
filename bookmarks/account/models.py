from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model


# Create your models here.
class Profile(models.Model):
    """Create extension on default user model"""
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/',
                              blank=True)

    def __str__(self):
        return f"Profile of {self.user.username}"


class Contact(models.Model):
    """Table for connecting other two, simulates connection Many-to-many"""
    # User that sent invite
    user_from = models.ForeignKey('auth.User',
                                  related_name='rel_from_set',
                                  on_delete=models.CASCADE)
    # User that accepts it
    user_to = models.ForeignKey('auth.User',
                                related_name='rel_to_set',
                                on_delete=models.CASCADE)
    # time when connection appears
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        """Modify behavior of indexing and order of Contact"""
        indexes = [
            models.Index(fields=['-created']),
        ]
        ordering = ['-created']

    def __str__(self):
        return f'{self.user_from} follows {self.user_to}'


# Add following field to User dynamically
user_model = get_user_model()
user_model.add_to_class('following',
                        models.ManyToManyField('self',
                                               through=Contact,
                                               related_name='followers',
                                               symmetrical=False))
