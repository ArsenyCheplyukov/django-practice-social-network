from django.contrib import admin
from .models import Image


# Register your models here.
@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    """Modify image appearance"""
    # define fields that admin can see
    list_display = ['title', 'slug', 'image', 'created']
    # define fields showing order
    list_filter = ['created']
3