from django import forms
from django.utils.text import slugify
from django.core.files.base import ContentFile
import requests
from .models import Image


class ImageCreateForm(forms.ModelForm):
    """Create form for passing new images to site"""
    class Meta:
        """Modify this form behavior"""
        model = Image
        fields = ['title', 'url', 'description']
        # Inportant: widgets as dictionary
        widgets = {
            'url': forms.HiddenInput,
        }

    def clean_url(self):
        """Try to filter url to determine is this contains image"""
        url = self.cleaned_data['url']
        valid_extensions = ['jpg', 'jpeg', 'png']
        extension = url.rsplit('.', 1)[1].lower()
        if extension not in valid_extensions:
            raise forms.ValidationError('The given URL does not '
                                        'match valid image extensions.')
        return url

    def save(self,
             force_insert=False,
             force_update=False,
             commit=True):
        """Try to save image from checked url"""
        # use image url checker from model
        image = super().save(commit=False)
        # get url from data
        image_url = self.cleaned_data['url']
        # make slug from title
        name = slugify(image.title)
        # get file extension from url (old approach)
        extension = image_url.rsplit('.', 1)[1].lower()
        # create name with extension to save to
        image_name = f'{name}.{extension}'
        # download image from current url
        response = requests.get(image_url)
        # save image data
        image.image.save(image_name,
                         ContentFile(response.content),
                         save=False)
        # if need to save image as model object - do it
        if commit:
            image.save()
        return image
