import datetime
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from .models import Action


def create_action(user, verb, target=None):
    """Create brief alias for creating and saving action"""
    # checks is some action performed in last minute by this user
    now = datetime.datetime.now()
    min_time = now - timezone.timedelta(seconds=60)
    similar_actions = Action.objects.filter(user=user,
                                            verb=verb,
                                            created__gte=min_time)
    if target:
        target_ct = ContentType.objects.get_for_model(target)
        similar_actions = similar_actions.filter(
            target_ct=target_ct,
            target_id=target.id,
        )
    if not similar_actions:
        # Not similar actions found
        action = Action(user=user, verb=verb, target=target)
        action.save()
        return True
    return False
