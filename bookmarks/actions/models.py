from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


# Create your models here.
class Action(models.Model):
    """Keed user actions model for history in site"""
    user = models.ForeignKey('auth.User',
                             related_name='actions',
                             on_delete=models.CASCADE)
    # maybe text of action
    verb = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    target_ct = models.ForeignKey(ContentType,
                                  blank=True,
                                  null=True,
                                  related_name='target_obj',
                                  on_delete=models.CASCADE)
    target_id = models.PositiveBigIntegerField(null=True,
                                               blank=True)
    # Summarize and manipulate relationships using the previous two fields
    target = GenericForeignKey('target_ct', 'target_id')

    class Meta:
        """Modify indexing and ordering behavior"""
        indexes = [
            models.Index(fields=['-created'])
        ]
        ordering = ['-created']
